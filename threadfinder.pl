#!/usr/bin/env perl -T

use strict;
use warnings;
use utf8;

use Data::Dumper;
use LWP::Simple;
use JSON;
use Irssi;
use Test::JSON;

my $latestthread = "No thread found";
my $board = "g";
my $searchterm = "cyberpunk";
my $channel = '#/g/punk';
my $separator = " || ";
my @topic;
 
sub find_thread{
    my $timestamp = 0;
    my $pages_json;
    do {
        $pages_json = get("http://a.4cdn.org/$board/catalog.json");
    }
    until (eval{ from_json $pages_json ;});

    my $pages = from_json($pages_json);
    my $pagecount = 0;
    my $threadcount = 0;
    my $pagemax = scalar @$pages - 1;
    for my $page (@$pages){
        my $threads = $page->{threads};
        for my $thread (@$threads){
            if (exists $thread->{sub} && $thread->{sub} =~ /$searchterm/i && $thread->{no} >= $timestamp){
                $timestamp = $thread->{no};
                return "Current thread: https://boards.4chan.org/$board/res/$thread->{no}";
            }
        }
    }
    return "No thread found";
}

sub find_rizon{
    my (@servers) = Irssi::servers();
    for my $server (@servers){
        if ($server->{chatnet} eq "Rizon"){
            return $server;
        }
    }
    return undef;
}

sub push_item{
    if (my $newitem = $_[0]){
        if ($newitem =~ m/^[\s\d\w\p{PosixPunct}^\\]/){
            # Remove leading and trailing whitespace (if any)
            $newitem =~ s/^\s+//;
            $newitem =~ s/\s+$//;
            push(@topic,$newitem);
            set_topic();
        } else {
            print "Invalid characters in topic";
        }
    }
}

sub pop_item{
    print scalar @topic;
    if(scalar @topic > 0){
        pop(@topic);
        set_topic();
    }
}

sub del_item{
    my ($element) = @_;
    print Dumper $element;
    if ($element =~ /^\d+$/ and $element >= 0 and $element < scalar @topic){
        splice @topic, $element, 1;
        set_topic();
    } else {print "failed check";}
}

sub ins_item{
    my ($element,$contents) = @_;
    if ($element =~ /^\d+$/ and $element > 0 and $contents){
        if ($contents =~ m/^[\s\d\w\p{PosixPunct}^\\]/){
            if ($element > scalar @topic){
                $element = scalar @topic;
            }
            splice @topic, $element, 0, $contents; 
            set_topic();
        } else {
            print "Invalid characters in topic";
        }
    }
}

sub check_op{
    my ($nick,$chan,$server) = @_;
    my @allowed = ("op","halfop","voice");
    foreach (@allowed){
        if (Irssi::channel_find($chan)->nick_find($nick)->{$_} == 1){
           return 1;
        }
    }
    return 0;
}

sub import_topic{
        my ($chan) = @_;
        my $curtopic = Irssi::channel_find($chan)->{topic};
        print $curtopic;
        my @curtopic_array = split(/ \|\| /,$curtopic);
        foreach (@curtopic_array){
            if ($_ =~ /^Current thread: https/ or $_ =~ /^No thread found$/){
                $latestthread = $_;
                $_ = "#THREAD#";
            }
        }
        @topic = @curtopic_array;
        print Dumper(@topic);
}


sub detect_topic_set{
    my ($server, $msg, $nick, $address, $chan);
    if (scalar @_ == 3){
        ($server, $msg, $chan) = @_;
        $address = $server->{userhost};
        $nick = $server->{nick};
    } else {
        ($server, $msg, $nick, $address, $chan) = @_;
    }
    if ($chan eq $channel and check_op($nick,$chan)){
        if ($msg =~ /^!push (.*)/){
            push_item($1);
        }
        if ($msg =~ /^!pop$/){
            pop_item();
        }
        if ($msg =~ /^!del (\d+)/){
            print "Calling del_item($1)";
            del_item($1);
        }
        if ($msg =~ /^!ins (\d+) (.*)/){
            print "Calling ins_item($1, $2)";
            ins_item($1,$2);
        }
        if ($msg =~ /^!import$/){
            import_topic($chan);
        }
    }
}

sub set_topic{
    my $server = find_rizon;
    my $topicstring = "";
    my $pos = 0;
    foreach my $item (@topic){
        if ($item =~ /^#THREAD#$/){
            $topicstring .= $latestthread;
        } else {
            $topicstring .= $item;
        }
        if ($pos < $#topic){
            $topicstring .= $separator;
        }
        $pos++;
    }
    $server->send_raw("TOPIC $channel :$topicstring");
}

sub update_thread{
    my $fetchedthread = find_thread;
    if ($fetchedthread ne $latestthread){
        $latestthread = $fetchedthread;
        print Dumper @topic;
        if ( grep (/^#THREAD#$/,@topic) ){
            set_topic();
        } else {
            print "shit not found";
        }
    }
}
Irssi::timeout_add(60000,"update_thread","");
Irssi::signal_add('message public','detect_topic_set');
Irssi::signal_add('message own_public','detect_topic_set');
